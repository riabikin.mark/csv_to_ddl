package mark;

import com.opencsv.CSVReader;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;


public class App {
    public static void main(String[] args) {

        String csvFile = args[0];//"C:\\Users\\User\\Downloads\\World_Development_Indicators\\gender_meta.csv";
        CSVReader reader = null;
        Vector<String> headers = new Vector<>();
        headers.add("country_name");
        headers.add("country_code");
        headers.add("time");
        headers.add("time_code");

        try {
            reader = new CSVReader(new FileReader(csvFile));
            String[] line;
            reader.readNext();
            while ((line = reader.readNext()) != null) {
                String tmp = line[2];
                tmp = tmp.replaceAll(" ", "_").toLowerCase();
                tmp = tmp.replaceAll("[^A-Z|a-z|0-9]", "_");
                tmp = tmp.replaceAll("__", "_");
                tmp = tmp.replaceAll("[_]$", "");
                tmp = tmp.replaceAll("[_]$", "");
                headers.add(tmp);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        //----------------------------------------------------------------------------------

        String[] types = new String[headers.size()];
        csvFile = args[1];//"C:\\Users\\User\\Downloads\\World_Development_Indicators\\gender.csv";
        try {
            reader = new CSVReader(new FileReader(csvFile));
            String[] line;
            reader.readNext();
            line = reader.readNext();
            String type = "null";
            //AWFULLLLLL
            for (int i = 0; i < line.length; i++) {
                while (type == "null") {
                    if (!line[i].isEmpty()) {
                        try {
                            Double.parseDouble(line[i]);
                            type = "double";
                        } catch (Exception e) {
                            try {
                                Integer.parseInt(line[i]);
                                type = "int";
                            } catch (Exception ee) {
                                type = "varchar(100)";
                            }
                        }
                    }
                    line = reader.readNext();
                }
                types[i] = type;
                type = "null";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.print("(" + headers.get(0) + "  " + types[0]);
        for (int i = 1; i < types.length; i++) {
            System.out.print(", \n" + headers.get(i) + "  " + types[i]);
        }
        System.out.println(")");

        String fileName = "out.txt";

        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(fileName, true));
            writer.append("\n\n");

            writer.append("(" + headers.get(0) + "  " + types[0]);
            for (int i = 1; i < types.length; i++) {
                writer.append(", \n" + headers.get(i) + "  " + types[i]);
            }
            writer.append(")");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
